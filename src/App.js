import React from 'react';
import './App.scss';
import 'swiper/swiper.min.css'
import './assets/boxicons-2.0.7/css/boxicons.min.css'

import { BrowserRouter, Outlet, Route, RouterProvider, Routes, createBrowserRouter } from 'react-router-dom';

import Header from './components/header/Header'
import Footer from './components/footer/Footer'
import Routing from './config/Routing';
import Catalog from './pages/Catalog';
import Home from './pages/Home';
import Detail from './pages/detail/Detail';

function App() {

  const Layout = () => {
    return (
      <>
      <Header/>
      <Outlet/>
      <Footer/>
      </>
    )
  }

  const router = createBrowserRouter([
    {
      path : '/',
      element : <Layout/>,
      children : [
        {
          path : '/',
          element : <Home/>
        },
        {
          path : '/:category',
          element : <Catalog/>
        },
        {
          path : '/:category/:id',
          element : <Detail/>
        },
        {
          path : '/:category/search/:keyword',
          element : <Catalog/>
        },
      ]
    }
  ])

  return (

    <div>
      <RouterProvider router={router}/>
    </div>



    // <BrowserRouter>
    //   <Route render={
    //     props => (
    //       <>
    //         <Header {...props} />
    //         <Routing/>
    //         <Footer/>
    //       </>
    //     )
    //   } />
    // </BrowserRouter>
    

    // <BrowserRouter>
    //   <Routes>
    //     <Route
    //       path="/"
    //       animate={true}
    //       element={
    //         <React.Fragment>
    //           <Header />
    //           <Home/>
    //           <Footer />
    //         </React.Fragment>
    //       }
    //     />

    //     <Route
    //       path="/movie"
    //       element={
    //         <React.Fragment>
    //           <Header />
    //           <Catalog />
    //           <Footer/>
    //         </React.Fragment>
    //       }
    //     />
    //   </Routes>
    // </BrowserRouter>
    

    // <BrowserRouter>
    // <Header />
    // <Routes>
    //   <Route path="/" element={<Home />}/>
    //   <Route path="/Catalog" element={<Catalog />}/>
    //   <Route path="/Detail" element={<Detail />}/>
    // </Routes>
    // <Footer />
    
    // </BrowserRouter>
  );
}

export default App;
